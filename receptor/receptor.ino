/*  TITULO: Comunicación serie entre dos Arduinos - Receptor_serie_RX
 
    AUTOR:
   
    MARIANO DEL CAMPO GARCÍA (@2016) --> INGENIERO TÉCNICO INDUSTRIAL ESPECIALIDAD ELECTRÓNICA
    - FACEBOOK: https://www.facebook.com/mariano.delcampogarcia
    - TWITTER: https://twitter.com/MarianoCampoGa
    - CORREO: marianodc83@gmail.com
   
   
    DESCRIPCIÓN DEL PROGRAMA
   
    Programa que recibe un carácter a través del puerto serie "mySerial" creado por software:
    - Si el carácter recibido es "H" se enciende el LED (pulsador presionado).
    - Si el carácter recibido es "L" se apaga el LED (pulsador no presionado).
   
   
    ESQUEMA DE CONEXION
   
                                      +-----+
         +----[PWR]-------------------| USB |--+
         |                            +-----+  |
         |         GND/RST2  [ ][ ]            |
         |       MOSI2/SCK2  [ ][ ]  A5/SCL[ ] |    
         |          5V/MISO2 [ ][ ]  A4/SDA[ ] |    
         |                             AREF[ ] |
         |                              GND[ ] |
         | [ ]N/C                    SCK/13[ ] |  
         | [ ]IOREF                 MISO/12[ ] |  
         | [ ]RST                   MOSI/11[ ]~|  
         | [ ]3V3    +---+               10[ ]~|   Pin TX del "Arduino Emisor"
         | [ ]5v    -| A |-               9[ ]~|  
         | [ ]GND   -| R |-               8[ ] |  
         | [ ]GND   -| D |-                    |
   +5V   | [ ]Vin   -| U |-               7[ ] |  
         |          -| I |-               6[ ]~|  
         | [ ]A0    -| N |-               5[ ]~|   LED(+)
         | [ ]A1    -| O |-               4[ ] |  
         | [ ]A2     +---+           INT1/3[ ]~|  
         | [ ]A3                     INT0/2[ ] |  
         | [ ]A4/SDA  RST SCK MISO     TX>1[ ] |  
         | [ ]A5/SCL  [ ] [ ] [ ]      RX<0[ ] |  
         |            [ ] [ ] [ ]              |
         |  UNO_R3    GND MOSI 5V  ____________/
          \_______________________/
 
  NOTAS:
   - Cátodo(-) del LED (pata más corta) a GND a través de una R=220 omhs.
   - Este Arduino se alimenta conectando el pin "Vin" a los +5V del "Arduino Emisor".
   - Las masas de los dos Arduinos tienen que estar unidas entre sí.
   
*/
 
  // Incluimos la librería
  #include <SoftwareSerial.h>
 
  #define LED  5  // Pin digital para el LED
  #define test 13
  char estado;
 
  // Declaro un nuevo puerto para la comunicación serie
  //SoftwareSerial mySerial(0, 1); // RX, TX
 
  void setup()
  {
    Serial.begin(9600); // Comienzo de la comunicación serie
    pinMode(LED, OUTPUT);   // Pin digital 5 como salida
  }
 
  void loop()
  {
    // Si por el puerto serie llegan datos (RX)
    if (Serial.available())
    {          
      
      // Almaceno el carácter que llega por el puerto serie (RX)
      estado = Serial.read();  
      
      // Si es una 'H'
      if (estado == 'H')          
      {
        // Enciendo el LED (nivel ALTO)
        digitalWrite(LED, HIGH);        
      }
     
      // Si es una 'L'
      if (estado == 'L')          
      {
        // Apago el LED (nivel BAJO)
        digitalWrite(LED, LOW);        
      }
    }
  }
