/*  TITULO: Comunicación serie entre dos Arduinos - Emisor_serie_TX
 
    AUTOR:
   
    MARIANO DEL CAMPO GARCÍA (@2016) --> INGENIERO TÉCNICO INDUSTRIAL ESPECIALIDAD ELECTRÓNICA
    - FACEBOOK: https://www.facebook.com/mariano.delcampogarcia
    - TWITTER: https://twitter.com/MarianoCampoGa
    - CORREO: marianodc83@gmail.com
   
   
    DESCRIPCIÓN DEL PROGRAMA
   
    Programa que envía un caracter "H" (presionado) o "L" (no presionado), a través del puerto serie
    "mySerial" creado por software, en función del estado en el que se encuentre el pulsador.
   
   
    ESQUEMA DE CONEXION
   
                                      +-----+
         +----[PWR]-------------------| USB |--+
         |                            +-----+  |
         |         GND/RST2  [ ][ ]            |
         |       MOSI2/SCK2  [ ][ ]  A5/SCL[ ] |    
         |          5V/MISO2 [ ][ ]  A4/SDA[ ] |    
         |                             AREF[ ] |
         |                              GND[ ] |
         | [ ]N/C                    SCK/13[ ] |  
         | [ ]IOREF                 MISO/12[ ] |  
         | [ ]RST                   MOSI/11[ ]~|   Pin RX del "Arduino Receptor"
         | [ ]3V3    +---+               10[ ]~|  
         | [ ]5v    -| A |-               9[ ]~|  
         | [ ]GND   -| R |-               8[ ] |  
         | [ ]GND   -| D |-                    |
         | [ ]Vin   -| U |-               7[ ] |  
         |          -| I |-               6[ ]~|  
         | [ ]A0    -| N |-               5[ ]~|  
         | [ ]A1    -| O |-               4[ ] |   PA
         | [ ]A2     +---+           INT1/3[ ]~|  
         | [ ]A3                     INT0/2[ ] |  
         | [ ]A4/SDA  RST SCK MISO     TX>1[ ] |  
         | [ ]A5/SCL  [ ] [ ] [ ]      RX<0[ ] |  
         |            [ ] [ ] [ ]              |
         |  UNO_R3    GND MOSI 5V  ____________/
          \_______________________/
 
  NOTAS:
 
   - Los pulsadores suelen tener dos pines, que vamos a denominar PA y PB (si es de 4 sólo usamos 2 de ellos)
   - Conexión PULL-DOWN del pulsador.
       - PB conectado a VCC.
       - PA conectado a GND a través de una R=10K ohms.
   - Las masas de los dos Arduinos tienen que estar unidas entre sí.
       
*/
 
  // Incluimos la librería
  #include <SoftwareSerial.h>
 
  const int Pulsador = 4;  // Pin digital para el pulsador
  int estadoPulsador = 0;  // Variable para ver el estado del pulsador
 
  // Declaro un nuevo puerto para la comunicación serie
  SoftwareSerial mySerial(0, 1); // RX, TX
 
  void setup()
  {
    mySerial.begin(9600); // Comienzo de la comunicación serie
    pinMode(Pulsador, INPUT);  // Pin digital 4 como entrada  
  }
 
  void loop()
  {
    // Lee y almacena el estado del pulsador
    estadoPulsador = digitalRead(Pulsador);
 
    // Si el pulsador está presionado
    if (estadoPulsador == HIGH)
    {                                
      mySerial.write('H'); // Enviamos 'H' por el puerto serie (TX)
    }
    else
    {                            
      mySerial.write('L'); // Enviamos 'L' por el puerto serie (TX)
    }

      }
